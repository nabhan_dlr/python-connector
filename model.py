from tortoise import fields, Tortoise, run_async
from tortoise.models import Model


class Author(Model):
    """
    This ORM mapping mirrors the creators in the elib dump
    """

    lineage = fields.CharField(max_length=300, default="")
    honourific = fields.CharField(max_length=300, default="")
    family = fields.CharField(max_length=300)
    given = fields.CharField(max_length=300)
    eprints: fields.ReverseRelation["Eprint"]

    class Meta:
        """
        Authors don't have a unique ID on elib.
        First + last name together are "unique enough" for now
        """

        unique_together = (("family", "given"),)

    def __str__(self):
        return f"{self.given}, {self.family}"

    @staticmethod
    async def get_all(first):
        await Tortoise.init(db_url="sqlite://db.sqlite3", modules={"models": ["model"]})
        return await Author.all().limit(first)


class Eprint(Model):
    """
    This ORM mapping mirrors the publications in the elib dump
    """

    eprintid = fields.IntField(pk=True)
    title = fields.TextField(default="")
    uri = fields.CharField(max_length=300, default="")
    abstract = fields.TextField(default="")
    datestamp = fields.DatetimeField(default="")
    authors: fields.ManyToManyRelation["Author"] = fields.ManyToManyField(
        "models.Author", related_name="eprints", through="eprint_author"
    )

    def __str__(self):
        return self.title

    @staticmethod
    async def get_all(offset, first):
        await Tortoise.init(db_url="sqlite://db.sqlite3", modules={"models": ["model"]})
        return await Eprint.filter(eprintid__gte=offset).limit(first)
