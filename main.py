import typing

import strawberry
from fastapi import FastAPI
from strawberry.fastapi import GraphQLRouter

from model import Author, Eprint


@strawberry.type
class ElibAuthor:
    lineage: str
    honourific: str
    family: str
    given: str


@strawberry.type
class ElibPublication:
    eprintid: int
    title: str
    uri: str
    abstract: str
    datestamp: str
    first: int
    authors: typing.List["ElibAuthor"]


@strawberry.type
class Query:
    @strawberry.field
    async def ElibPublication(
        self, offset: int, first: int
    ) -> typing.List[ElibPublication]:
        return await Eprint.get_all(offset, first)

    @strawberry.field
    async def ElibAuthor(self, first: int) -> typing.List[ElibAuthor]:
        return await Author.get_all(first)


app = FastAPI()
app.include_router(
    GraphQLRouter(strawberry.Schema(query=Query)), prefix="/graphql"
)  # access to GraphiQL interface
app.include_router(
    GraphQLRouter(strawberry.Schema(query=Query), graphiql=False),
    prefix="/api/v0/connector/data",
)
